# Tienda

Este proyecto fue genedaro con la versión de Angular version 11.2.7 [Angular CLI](https://github.com/angular/angular-cli) .

## Development server

1. Ejecutar `ng serve -o` para correr el servidor. Esto navegará automáticamente a `http://localhost:4200/`.
2. Ejecutar `json-server --watch server/database.json` o `npm run server` para montar el Web Server.

## Build

Ejecutar `ng build` para construir el proyecto. Los archivos serán guardados en el directorio `dist/`.


## Further help

Para más ayuda en Angular CLI usar `ng help` o ir a la página [Angular CLI Overview and Command Reference](https://angular.io/cli).
